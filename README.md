### What is this repository for? ###

This repo lists the services upon which videowiki is based on

### Services ###
- Backend API ( https://bitbucket.org/HassanAmin9/tvw_backend )
- Frontend ( https://bitbucket.org/HassanAmin9/tvw_frontend )
- Exporter ( https://bitbucket.org/HassanAmin9/tvw_exporter )
- Transcriber ( https://bitbucket.org/HassanAmin9/tvw_transcriber )
- Spleeter integration ( https://bitbucket.org/HassanAmin9/tvw_spleeter )
- Audio Processor ( https://bitbucket.org/HassanAmin9/tvw_audio_processor )
- Translator ( https://bitbucket.org/HassanAmin9/tvw_translator )
